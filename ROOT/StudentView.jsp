<!DOCTYPE html>
<html>

<head>
	<title>Student View</title>
	<style>
		table,
		th,
		td {
			border: 1px solid black;
		}

		table>thead>tr>th,
		table>tbody>tr>td {
			padding: 3px 5px;
		}
	</style>
</head>

<body>
	<h1>Student View</h1>
	<table>
		<thead>
			<tr>
				<th>Name</th>
				<th>NIC</th>
				<th>Gender</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><% out.println("Hasanta"); %></td>
				<td><% out.println("123456789V"); %></td>
				<td><% out.println("Male"); %></td>
			</tr>
		</tbody>
	</table>
</body>

</html>