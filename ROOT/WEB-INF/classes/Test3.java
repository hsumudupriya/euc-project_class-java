import java.io.*;
import javax.servlet.http.*;
import javax.servlet.*;

public class Test3 extends HttpServlet {
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        PrintWriter out = res.getWriter();
        out.println("<h1>Test-3 : Dynamic Web Page using servlet</h1>");
        out.close();
    }
}