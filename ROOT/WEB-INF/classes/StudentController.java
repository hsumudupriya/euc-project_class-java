import java.io.*; // IOException
import javax.servlet.http.*; // HttpServletRequest, HttpServletResponse
import javax.servlet.*; // ServletException

public class StudentController extends HttpServlet {
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        PrintWriter out = res.getWriter();

        out.println("<!DOCTYPE html>");
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Student View</title>");
        out.println("<style>");
        out.println("table, th, td { border: 1px solid black; }");
        out.println("table>thead>tr>th, table > tbody > tr > td { padding: 3px 5px; }");
        out.println("</style>");
        out.println("</head>");
        out.println("<body>");
        out.println("<h1>Student View</h1>");
        out.println("<table>");
        out.println("<thead>");
        out.println("<tr>");
        out.println("<th>Name</th>");
        out.println("<th>NIC</th>");
        out.println("<th>Gender</th>");
        out.println("</tr>");
        out.println("</thead>");
        out.println("<tbody>");
        out.println("<tr>");
        out.println("<td>");
        out.println("Hasanta");
        out.println("</td>");
        out.println("<td>");
        out.println("123456789V");
        out.println("</td>");
        out.println("<td>");
        out.println("Male");
        out.println("</td>");
        out.println("</tr>");
        out.println("</tbody>");
        out.println("</table>");
        out.println("</body>");
        out.println("</html>");

        out.close();
    }
}
